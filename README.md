# Script para archlinux
- Scripts para facilitar tareas en arch


---
##### Descripcion: 
1. **update-arch.py** :  actualiza de forma automatica cada 7 dias una vez iniciado atraves del demonio.
   - **ejecucion** : 
     - `sudo systemctl enable update-arch.service && sudo systemctl start update-arch.service`  
2. **bitcoin-cli.py** :  interactuar con cli de bitcoin con la configuracion de  /etc/bitcoin/bitcoin.conf
   - **ejecucion**: recibe las mismas opciones que **bitcoin-cli**
     - `bitcoin-cli.py  opciones`
  
3. **i3exit.py** : GUI funciones de apagado del equipo
   
4. **rescue-grub-gpt.py** : recupera el grub en una tabla de particiones GPT y se le pasas la c como parametro actualizara la tabla de particiones.
   - **ejecucion**:
     - `sudo rescue-grub-gpt.py`  para recuperar el grub en modo **UEFI**
     - `sudo rescue-grub-gpt.py c`  para actualizar la tabla de particiones del grub
  
5. **menu.py** : menu depende de rofi para funcionar.
   - **Ejecucion i3wm**: esta configuracion se aplica en el archivo **config**
     - `bindsym $mod+z exec menu.py`
  
6. **rescue-system.py** : reinstala todos los paquetes del sistema
   1. si su sistema no inicia o inicia sin conexion a intertet puede usar una iso de archlinux montar todas las particiones, logearse al sistema montado y ejecutar el scrip
   2.  esto es util cuando borras carpetas del sistema y no sabes a que programa pertenece.
   3. todos los paquetes que no se encuentran en los repositorios seran almacenados en un archivo que se mostrara al final de ejecutar el script.
   4. el script puede tardar un tiempo en completar la accion depende de la catidad de paquetes y la coneccion a internet.
   5. luego de que termine puedes reiniciar el equipo.
   - **Ejecucion**: 
      -  `sudo rescue-system.py`
  
7. **gpg-decrypt.py** : Desencripta una archivo o carpeta usando GPG
   - **Ejecucion**: 
     - `gpg-decrypt.py file.gpg`
  
8. **gpg-encrypt.py**: Encripta un archivo o carpeta usando GPG, el proceso puede tardar dependiendo del tamaño del archivo o directorio 
   - **Ejecucion**:  
     - `gpg-encrypt.py file`
     - `gpg-encrypt.py dir`
  
9.  **gpg-read-plain-text.py**: Muestra la informacion de un archivo de texto plano sin desencriptarlo
    - **Ejecucion**: el archivo debe tener una sola capa de encriptado
      - `gpg-read-plain-text.py file.gpg`

9. **laravel-echo-server.py**: script personalizado para laravel echo server
    - **Ejecucion**
      - `laravel-echo-server.py config`  : permite generar una conficuracion la cual se ubicará en `/var/local/laravel-echo-server`
      - `systemctl enable laravel-echo-server.service` : habilitara el demonio para que se inicie automaticamente
      - `systemctl start laravel-echo-server.service` : iniciara el demonio 
### Dependencias requeridas arch 
`sudo pacman -S wipe rofi bzip2 python python-pip`
 
### Dependencias 
 - paquete requeridos
   - `os time subprocess datetime sys PyQt5 pynput`
 - puedes instalarlas usando `pip instal paquete`  
 - si tienes algun error de este tipo **error: externally-managed-environment** puedes utilizar la siguiente opcion para instalar los paquetes 
   - `pip instal paquete --break-system-packages`  

#!/usr/bin/env python3

import os
import shutil 
import sys
import subprocess
 

class Color:

    @staticmethod
    def black_color():
        return "\033[0;30m"

    @staticmethod
    def red_color():
        return "\033[0;31m"

    @staticmethod
    def green_color():
        return "\033[0;32m"

    @staticmethod
    def yellow_color():
        return "\033[0;33m"

    @staticmethod
    def blue_color():
        return "\033[0;34m"

    @staticmethod
    def magenta_color():
        return "\033[0;35m"

    @staticmethod
    def cian_color():
        return "\033[0;36m"

    @staticmethod
    def white_color():
        return "\033[0;37m"

    @staticmethod
    def reset_color():
        return "\033[0m"
 

SCRIPT_DIR = "bin"
INSTALL_SCRIPT = "/usr/local/bin"
SERVICES_DIR = "services"
INSTALL_SERVICES = "/usr/lib/systemd/system"
THEMES_DIR = "themes"
INSTALL_THEMES = "/usr/share/rofi/themes"

# Verificar permisos de superusuario
if os.geteuid() != 0:
    print("Este script debe ejecutarse con permisos de superusuario.") 
    command = f"sudo {sys.argv[0]}"
    subprocess.run(command, shell=True) 
    sys.exit()
    
scripts = os.listdir(SCRIPT_DIR) 

for scrip in scripts:
    print(f'> {Color.magenta_color()}{scrip[:-3]}{Color.reset_color()}')
 
script_selected = input(f"{Color.cian_color()}Escribe el nombre del Script a instalar \n{Color.yellow_color()}Para instalar todos presiona Enter:{Color.reset_color()} ")
 
def install(local, dest, CHMOD=0o755):
    script_files = os.listdir(local)
    for file in script_files:
        src_file = os.path.join(local, file)
        dst_file = os.path.join(dest, file)
       
        if script_selected in file  :
            try:
                if os.path.isfile(src_file):
                    shutil.copy(src_file, dst_file)
                    os.chmod(dst_file, CHMOD)
                print(f"\nMoviendo de {src_file} a  {dst_file}")
            except Exception as e:
                print(
                    f"Error al copiar el archivo {src_file} a {dst_file}\n Descripcion : {str(e)}")
       
install(SCRIPT_DIR, INSTALL_SCRIPT)
install(SERVICES_DIR, INSTALL_SERVICES, 0o644)
install(THEMES_DIR, INSTALL_THEMES, 0o644)

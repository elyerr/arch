#!/usr/bin/env python3
import os
import time
import subprocess
import sys

# Intervalo de actualización en segundos {(horas * dias) * minutos * segundos}
UPDATE_INTERVAL = (24 * 7) * 60 * 60
# Ruta del archivo de registro de la última actualización
LAST_UPDATE_FILE = '/var/local/update_arch/last_update'

# Ruta de paquetes descargados durante la actualización
CACHE_DIR = '/var/cache/pacman/pkg'


def update_arch(current_time, last_update):
    """Ejecuta los comandos para actualizar Arch Linux y luego elimina todos los paquetes descargados.
    """
    process = subprocess.run('pacman -Syy', shell=True, capture_output=True)
    
    if process.returncode != 0:
        send_notification("Error al actualizar, se intentara actualizar en 30 minutos", "dialog-error")
        # tiempo_actual + (minutos * segundos)
        if last_update != 0:
            set_last_update_time(last_update + (30 * 60))
        else:
            set_last_update_time((current_time - UPDATE_INTERVAL ) + (30 * 60))

    else:
        os.system('pacman -Syu --noconfirm')
        files = os.listdir(CACHE_DIR)
        for f in files:
            os.remove(os.path.join(CACHE_DIR, f))
        set_last_update_time(current_time)
        if last_update > 0:
            message = f'Arch Linux se ha actualizado correctamente.'
        else:
            message = 'No se encontraron registros de actualización previos.'
        send_notification(message)

def get_last_update_time():
    """Obtiene la fecha de la última actualización.

    Returns:
        float: Tiempo en segundos.
    """
    if os.path.exists(LAST_UPDATE_FILE):
        with open(LAST_UPDATE_FILE, 'r') as file:
            last_update = float(file.read())
        return last_update
    else:
        return 0


def set_last_update_time(timestamp):
    """Establece la hora de la última actualización.

    Args:
        timestamp (float): Fecha actual.
    """
    os.makedirs(os.path.dirname(LAST_UPDATE_FILE), exist_ok=True)
    with open(LAST_UPDATE_FILE, 'w') as file:
        file.write(str(timestamp))


def disable_sleep():
    """Deshabilita temporalmente sleep, suspend, hibernate para evitar errores en la actualización.
    """
    subprocess.run(['systemctl', 'mask', 'sleep.target',
                    'suspend.target', 'hibernate.target'])


def enable_sleep():
    """Habilita sleep, suspend, hibernate.
    """
    subprocess.run(['systemctl', 'unmask', 'sleep.target',
                    'suspend.target', 'hibernate.target'])


def timedate(time):
    """Convierte el tiempo en una forma legible para el usuario.

    Args:
        time (float): Tiempo en segundos.

    Returns:
        str: Tiempo en formato HH:MM:SS.
    """
    horas = int(time // 3600)
    minutos = int((time % 3600) // 60)
    segundos = int(time % 60)
    return f'{horas:02d}:{minutos:02d}:{segundos:02d}'


def get_users():
    """Obtiene los usuarios logeados.

    Returns:
        list: Lista de usuarios conectados al equipo.
    """
    users = []
    command = "ls /run/user"
    process = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True)
    output, error = process.communicate()

    if output:
        user_ids = output.decode('utf-8').split()

        for user_id in user_ids:
            command = f"awk -F':' '$3 == {user_id} {{ print $1 }}' /etc/passwd"
            process = subprocess.Popen(
                command, stdout=subprocess.PIPE, shell=True)
            output, error = process.communicate()

            if output:
                user_names = output.decode('utf-8').split()
                users.extend(user_names)
                
    return users


def send_notification(message, level="dialog-information"):
    """Envía una notificación a todos los usuarios conectados.

    Args:
        message (str): Mensaje a mostrar.
        level (str, optional): Nivel de alerta del mensaje. Defaults to "dialog-information".
    """
    for user in get_users():
        su_command = f'su {user} -c "DISPLAY=:0 DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/$(id -u {user})/bus notify-send \'Notificación de actualización\' \'{message}\' -i {level}"'
        os.system(su_command)


if __name__ == '__main__':
    """Ejecución del script.
    """
    while True:
        last_update = get_last_update_time()
        current_time = time.time()
        time_since_last_update = current_time - last_update

        if time_since_last_update >= UPDATE_INTERVAL:
            try:
                message = "Proceso de actualización iniciando en 5 segundos...\nPor favor, verifique que el equipo esté conectado a una fuente de alimentación y no apague el equipo hasta que el proceso termine."
                send_notification(message, "dialog-warning")
                time.sleep(5)
                disable_sleep()
                update_arch(current_time, last_update)    
                            
            except Exception as e:
                send_notification(
                    f'Se produjo un error al actualizar Arch Linux: {str(e)}', 'dialog-error')
            finally:
                enable_sleep()

        else:
            remaining_time = UPDATE_INTERVAL - time_since_last_update

            if remaining_time < 3600:  # Menos de 1 hora
                if remaining_time < 600:  # Menos de 10 minutos
                    send_notification(
                        f"Quedan {timedate(remaining_time)} minutos para la próxima actualización. Por favor, asegúrese de guardar su trabajo.",
                        "dialog-warning")
                    time.sleep(60)  # Notificar cada 1 minuto
                else:
                    send_notification(
                        f"Quedan {timedate(remaining_time)} minutos para la próxima actualización.")
                    time.sleep(300)  # Notificar cada 5 minutos
            else:
                if remaining_time > 43200:  # Más de 12 horas
                    send_notification(
                        f"Quedan {timedate(remaining_time)} horas para la próxima actualización")
                    time.sleep(14400)  # Notificar cada 4 horas
                else:
                    send_notification(
                        f"Quedan {timedate(remaining_time)} horas para la próxima actualización")
                    time.sleep(7200)  # Notificar cada 2 horas

        # Volver a ejecutar el script
        os.execv(__file__, sys.argv)

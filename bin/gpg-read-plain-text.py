#!/usr/bin/env python3

import subprocess
import sys
import os
from tkinter import * 

# Clase principal
class ReadFile:
    def __init__(self):
        self.file = None
        if len(sys.argv) > 1:
            self.file = sys.argv[1]

    # Lee el archivo encriptado
    def read_file(self):
        PATH = os.getcwd()
        if self.file is not None:
            command = f'gpg -d {os.path.join(PATH, self.file)}'
            output = subprocess.run(
                command, shell=True, capture_output=True, text=True)
            return f"{output.stderr}\n{output.stdout}"

        else:
            return 'Debes igresar la ruta del archivo como parametro en scrip\nEjemplo\n  read-encrypt.py  file.gpg'


# Interfaz de Tkinter
class ShowFile(Frame):
    def __init__(self, master, **kwargs):
        super().__init__(master)
        self.pack()        
        self.readFile = ReadFile()
        self.texto = self.readFile.read_file()
        
        yscoll = Scrollbar(master)
        yscoll.pack(side=RIGHT, fill=Y, padx=5, pady=5) 

        self.text_area = Text(
            master, width=kwargs['width'], height=kwargs['height'])
        self.text_area.insert(END, self.texto)
        self.text_area.pack()
        
        yscoll.configure(command=self.text_area.yview)
        
        self.text_area.configure(yscrollcommand=yscoll.set) 
        

# Ejecución del programa
if __name__ == '__main__':
    root = Tk()
    screen_width = root.winfo_screenwidth()
    screen_height = root.winfo_screenheight()
    app = ShowFile(root, width=screen_width, height=screen_height)
    app.master.title('READ FILE ENCRYPT')
    app.mainloop()

#!/usr/bin/env python3

import os
import subprocess
import sys


class FColor:

    @staticmethod
    def black_color():
        return "\033[0;30m"

    @staticmethod
    def red_color():
        return "\033[0;31m"

    @staticmethod
    def green_color():
        return "\033[0;32m"

    @staticmethod
    def yellow_color():
        return "\033[0;33m"

    @staticmethod
    def blue_color():
        return "\033[0;34m"

    @staticmethod
    def magenta_color():
        return "\033[0;35m"

    @staticmethod
    def cian_color():
        return "\033[0;36m"

    @staticmethod
    def white_color():
        return "\033[0;37m"

    @staticmethod
    def reset_color():
        return "\033[0m"


class DecryptFile:

    def __init__(self):
        subprocess.run('clear', shell=True)
        self.file = None
        if len(sys.argv) > 1:
            self.file = sys.argv[1]

        self.file_exists()
        self.decrypt_file()
        

    # Verificar existencia del archivo
    def file_exists(self):
        try:
            if not os.path.exists(self.get_path()):
                print(f'{FColor.red_color()}El archivo {FColor.white_color()}"{self.file}"{FColor.reset_color()}{FColor.red_color()} no existe{FColor.reset_color()}')
                sys.exit(0)
            return self.get_path()
        except TypeError as e:
            print(
                f'{FColor.red_color()}Ingresa el archivo encriptado{FColor.reset_color()}')
            sys.exit(0)
    
    #descomprime carpetas
    def uncompress(self):         
        if self.file[len(self.file)-8:] == '.tar.bz2':
            cmd = f'tar -xjf {self.file}'
            subprocess.run(cmd, shell=True)            
            self.wipe(self.get_path(), False)
            print(
                f'{FColor.magenta_color()}La carpeta ha sido desencriptada correctamente!!{FColor.reset_color()}\n')
              
    def get_path(self):
        return os.path.join(os.getcwd(), self.file)
        
    # verifica que el archivo este encriptado
    def is_file_encrypted(self):
        command = f'gpg --list-packets {self.file}'
        out = subprocess.run(command, shell=True,
                             capture_output=True, text=True)
        if out.returncode == 0:
            return True
        return False
     
        
    # verificar si esta encriptado para desencriptar
    def decrypt_file(self): 
        #descomprimir si es carpeta
        self.uncompress()
        if (self.is_file_encrypted()):
            file_name_decrypted = self.file[:-4]
            command = f'gpg --output {file_name_decrypted} --decrypt {self.file}'
            out = subprocess.run(command, shell=True, capture_output=True, text=True)
            if out.returncode == 0:
                print(f'{FColor.magenta_color()}Archivo desenciptado correctamente!{FColor.reset_color()}\n')
                self.wipe(self.get_path())
                #actuliza nombre del archivo
                self.file = file_name_decrypted
                # recursion si el archivo sigue encriptado
                self.decrypt_file()
        else:
            print(
                f'{FColor.red_color()}El archivo no se encuentra ecriptado{FColor.reset_color()}\n')

    # implementacion de borrado
    def wipe(self, PATH, is_file=True):    
        if is_file:
            command = f'wipe -ZdNTVEAkO -S512 -C4096 -l2 -x32 -p32 {PATH}'
        else:
            command = f'wipe -rf -ZdNTVEAkO -S512 -C4096 -l2 -x32 -p32 {PATH}'        
        subprocess.run(command, shell=True)


if __name__ == '__main__':
    DecryptFile()

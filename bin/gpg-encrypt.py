#!/usr/bin/env python3

import os
import subprocess
import sys
import time
from pynput import keyboard


class FColor:

    @staticmethod
    def black_color():
        return "\033[0;30m"

    @staticmethod
    def red_color():
        return "\033[0;31m"

    @staticmethod
    def green_color():
        return "\033[0;32m"

    @staticmethod
    def yellow_color():
        return "\033[0;33m"

    @staticmethod
    def blue_color():
        return "\033[0;34m"

    @staticmethod
    def magenta_color():
        return "\033[0;35m"

    @staticmethod
    def cian_color():
        return "\033[0;36m"

    @staticmethod
    def white_color():
        return "\033[0;37m"

    @staticmethod
    def reset_color():
        return "\033[0m"


class EncryptFIle():

    def __init__(self): 
        # evento
        self.key_pressed = False
        self.listener = None 
        # file
        self.file = None
        if len(sys.argv) > 1:
            self.file = sys.argv[1]

        self.get_path()
        self.compress_dir()
        self.showKeyGPG()

    def get_path(self):
        """Verifica que el archivo a encriptar exista

        Returns:
            _type_: _description_
        """
        try:
            path = os.path.join(os.getcwd(), self.file)
            if not os.path.exists(path):
                print(f'{FColor.red_color()}El archivo {FColor.white_color()}"{self.file}"{FColor.reset_color()}{FColor.red_color()} no existe{FColor.reset_color()}')
                sys.exit(0)
            return path
        except TypeError as e:
            print(
                f'{FColor.red_color()}Ingresa el archivo a encriptar{FColor.reset_color()}')
            sys.exit(0)

    def compress_dir(self):
        """comprime la carpeta antes de encriptar
        """
        self.remove_slash()
        if os.path.isdir(self.file):
            # comprimir carpeta
            cmd = f'tar -cjf {self.file}.tar.bz2 {self.file}'
            out = subprocess.run(
                cmd, shell=True, capture_output=True, text=True)
            # capturar nombre
            if out.returncode == 0:
                # remover carpeta
                self.wipe(self.get_path(), False)
                # actualizamos el archivo
                self.file = f'{self.file}.tar.bz2'

    def ask_for_key(self, entered_key):
        """usado para confirmar la clave GPG

        Args:
            key_pressed (bool): retorna true or false
        """        
        if entered_key == keyboard.Key.enter or str(entered_key) == "'y'":
            self.key_pressed = True
            self.listener.stop() 
        else:
            self.listener.stop()

    def remove_slash(self):
        """remueve el slash del archivo que se le pase
        """
        if self.file[len(self.file)-1] == '/':
            self.file = self.file[:-1]

    def showKeyGPG(self):
        """Mustras las claves GPG y ejecuta la encriptacion
        """
        subprocess.run('clear', shell=True)
        list_keys = subprocess.run(
            ['gpg', '--list-keys'], capture_output=True, text=True)
        print(list_keys.stdout) 
        
        self.KEY = input(
            f'{FColor.white_color()}Ingresa una clave para encriptar tus archivos : {FColor.reset_color()}')
        
        if (self.verified_is_key_exists(self.KEY).returncode == 0):
            #solicitud para confirmacion de clave 
            print(f'\n{FColor.cian_color()}Clave Ingresada:{FColor.reset_color()}')
            print(self.verified_is_key_exists(self.KEY).stdout)
            print(f'{FColor.yellow_color()}Por favor confirma si la clave es correcta\nY : Y or Enter\nQ: Q para cancelar el proceso{FColor.reset_color()}')
            self.listener = keyboard.Listener(on_press=self.ask_for_key)
            self.listener.start()  # inicia el evento
            self.listener.join()  # espera del evento
        
            if self.key_pressed: 
                self.encrypt_file(self.KEY)
            else: 
                print(
                    f'\n{FColor.magenta_color()}\nTerminando ejecucion del programa ...{FColor.reset_color()}')
                time.sleep(2)
                sys.exit()
        else:
            print(
                f"{FColor.red_color()}Por Favor debe ingresar una clave valida!!!{FColor.reset_color()}")

    def verified_is_key_exists(self, key):
        """Verificar existencia de la KEY GPG

        Args:
            key (str): Clave GPG

        Returns:
            list: lista de datos
        """
        return subprocess.run(['gpg', '--list-key', key], capture_output=True, text=True)

    def encrypt_file(self, KEY):
        """Encripta el  archivo usando tu KEY GPG

        Args:
            KEY (str): Recibe la llave que se usara para encriptar
        """
        command = f'gpg --cipher-algo AES256  -e -r {KEY} {self.file}'
        subprocess.run(command, shell=True, capture_output=True, text=True)

        print(
            f'\n{FColor.magenta_color()}Archivo encriptado correctamente!{FColor.reset_color()}\n')
        self.wipe(self.get_path())

    def wipe(self, PATH, is_file=True):
        """Elimina de forma seguro al archivo encriptado

        Args:
            PATH (str): ruta del archivo sin encriptar.
            is_file (bool, optional): verificar que sea un archivo o carpeta
        """
        if is_file:
            command = f'wipe -ZdNTVEAkO -S512 -C4096 -l2 -x32 -p32 {PATH}'
        else:
            command = f'wipe -rf -ZdNTVEAkO -S512 -C4096 -l2 -x32 -p32 {PATH}'
        subprocess.run(command, shell=True)


# Ejecuta el script
if __name__ == '__main__':
    EncryptFIle()

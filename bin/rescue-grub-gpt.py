#!/usr/bin/env python3

import subprocess
import getpass
import os
import sys

CONFIG = None 
if len(sys.argv) > 1:
    CONFIG = sys.argv[1]

def install_grub():
    subprocess.run(["grub-install", "--target=x86_64-efi",
                   "--efi-directory=/boot/efi", "--debug", "--recheck"])


def generate_grub_config():
    subprocess.run(["grub-mkconfig", "-o", "/boot/grub/grub.cfg"])


if __name__ == "__main__":
        #se solicita contraseña si no tiene
        if os.geteuid() != 0:
            print("Este script debe ejecutarse con permisos de superusuario.")
            password = getpass.getpass(
                "Por favor, ingresa la contraseña de superusuario:")
            command = f"sudo -S python3 {os.path.abspath(__file__)} {CONFIG}"
            os.system(f'echo "{password}" | {command}')
            sys.exit()

        #ejecutara solo mkconfig
        if CONFIG is not None and CONFIG == 'c':
             generate_grub_config()
             sys.exit()
             
        install_grub()
        generate_grub_config()

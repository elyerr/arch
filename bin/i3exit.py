#!/usr/bin/env python3

import sys
import subprocess
from PyQt5.QtWidgets import QApplication, QMainWindow, QPushButton, QMessageBox
from PyQt5.QtGui import QFont, QPalette, QColor
from PyQt5.QtCore import Qt, QEvent, QObject


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("i3exit")
        self.setFixedSize(300, 400)
        self.font = QFont("Helvetica", 12)

        self.logout_button = self.create_button(
            "Logout (L)", handle_logout, 50, 50, Qt.Key_L)
        self.switch_user_button = self.create_button(
            "Switch User (S)", handle_switch_user, 50, 100, Qt.Key_S)
        self.suspend_button = self.create_button(
            "Suspend (U)", handle_suspend, 50, 150, Qt.Key_U)
        self.hibernate_button = self.create_button(
            "Hibernate (H)", handle_hibernate, 50, 200, Qt.Key_H)
        self.reboot_button = self.create_button(
            "Reboot (R)", handle_reboot, 50, 250, Qt.Key_R)
        self.shutdown_button = self.create_button(
            "Shutdown (O)", handle_shutdown, 50, 300, Qt.Key_O)

    def create_button(self, text, handler, x, y, key):
        button = QPushButton(text, self)
        button.setFont(self.font)
        button.setGeometry(x, y, 200, 40)
        button.clicked.connect(handler)
        button.setShortcut(key)
        button.installEventFilter(
            ButtonHoverFilter(button, "#1f497d", "white"))
        return button

    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Escape:
            self.close()


class ButtonHoverFilter(QObject):
    def __init__(self, button, bg_color, text_color):
        super().__init__()
        self.button = button
        self.bg_color = bg_color
        self.text_color = text_color
        self.original_palette = button.palette()

    def eventFilter(self, obj, event):
        if event.type() == QEvent.Enter:
            palette = self.button.palette()
            palette.setColor(QPalette.Button, QColor(self.bg_color))
            palette.setColor(QPalette.ButtonText, QColor(self.text_color))
            self.button.setPalette(palette)
        elif event.type() == QEvent.Leave or event.type() == QEvent.FocusOut:
            self.button.setPalette(self.original_palette)
        return super().eventFilter(obj, event)


def handle_logout():
    reply = QMessageBox.question(
        window, "Logout", "Are you sure you want to logout?", QMessageBox.Yes | QMessageBox.No)
    if reply == QMessageBox.Yes:
        subprocess.run(["i3-msg", "exit"])
    window.close()


def handle_switch_user():
    reply = QMessageBox.question(
        window, "Switch User", "Are you sure you want to switch user?", QMessageBox.Yes | QMessageBox.No)
    if reply == QMessageBox.Yes:
        subprocess.run(["dm-tool", "switch-to-greeter"])
    window.close()


def handle_suspend():
    reply = QMessageBox.question(
        window, "Suspend", "Are you sure you want to suspend?", QMessageBox.Yes | QMessageBox.No)
    if reply == QMessageBox.Yes:
        logind = "systemctl" if "systemd" in open(
            "/proc/1/comm").read() else "loginctl"
        subprocess.run([logind, "suspend"])
    window.close()


def handle_hibernate():
    reply = QMessageBox.question(
        window, "Hibernate", "Are you sure you want to hibernate?", QMessageBox.Yes | QMessageBox.No)
    if reply == QMessageBox.Yes:
        logind = "systemctl" if "systemd" in open(
            "/proc/1/comm").read() else "loginctl"
        subprocess.run([logind, "hibernate"])
    window.close()


def handle_reboot():
    reply = QMessageBox.question(
        window, "Reboot", "Are you sure you want to reboot?", QMessageBox.Yes | QMessageBox.No)
    if reply == QMessageBox.Yes:
        logind = "systemctl" if "systemd" in open(
            "/proc/1/comm").read() else "loginctl"
        subprocess.run([logind, "reboot"])
    window.close()


def handle_shutdown():
    reply = QMessageBox.question(
        window, "Shutdown", "Are you sure you want to shutdown?", QMessageBox.Yes | QMessageBox.No)
    if reply == QMessageBox.Yes:
        logind = "systemctl" if "systemd" in open(
            "/proc/1/comm").read() else "loginctl"
        subprocess.run([logind, "poweroff"])
    window.close()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    sys.exit(app.exec_())

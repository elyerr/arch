#!/usr/bin/env python3
#
#  Laravel echo server
#
#
#
import subprocess
import os
import shutil
import sys

DIR = '/var/local/laravel-echo-server'
CONFIG = 'laravel-echo-server.json'

# crea la configuracion


def create_config():
    if not os.path.exists(DIR):
        os.mkdir(DIR)

    subprocess.run('laravel-echo-server configure', shell=True)
    shutil.copy(CONFIG, os.path.join(DIR, CONFIG))


# ejecuta el servicio
def run_service():
    if not os.path.exists(os.path.join(DIR, CONFIG)):
        create_config()

    subprocess.run(
        f'laravel-echo-server start --config={os.path.join(DIR, CONFIG)}', shell=True)

##ejecucion del script
if __name__ == "__main__":

    if os.geteuid() != 0:
        print("Este script debe ejecutarse con permisos de superusuario.")        
        command = f"sudo {sys.argv[0]}"
        subprocess.run(command, shell=True)
        sys.exit()

    try:
        if sys.argv[1] == "config":
            create_config()

        if sys.argv[1] == "run":
            run_service()

    except IndexError as e:
        text = "\n\033[0;35mPor favor intrododuce los parametros correspondientes\033[0m\n"
        text += "\033[0;36mlaravel-echo-server config\033[0m : generara un archivo de configuracion\n"
        text += "\033[0;36mlaravel-echo-server run\033[0m : ejecuta el servicio\n"
        print(text)

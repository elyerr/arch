#!/usr/bin/env python3
import subprocess
import sys


def run_bitcoin_cli(*args):
    command = ['bitcoin-cli', '--conf=/etc/bitcoin/bitcoin.conf'] + list(args)
    try:
        result = subprocess.run(command, capture_output=True, text=True)
        output = result.stdout.strip()
        print(output)
    except subprocess.CalledProcessError as e:
        print(f"Error: {e}")


# Obtener los parámetros pasados al script (excluyendo el nombre del script)
args = sys.argv[1:]

# Ejecutar bitcoin-cli con los parámetros proporcionados
run_bitcoin_cli(*args)

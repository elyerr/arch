#!/usr/bin/env python3
 
import subprocess 
import os

import subprocess
 
# obtiene todos los paquetes installados
def getAllInstalledPackage():
    return "pacman -Qq"

# actuliza repositorios
def updateSystemKeys():
    command = ['pacman', '-Syy', 'archlinux-keyring', '--noconfirm']
    subprocess.run(command)
    
#genera un archivo con los paquetes de repositorios externos
def exportPackageNotFound(pkg):
    subprocess.run(f'echo {pkg} >> ~/package_not_found')

# verifica que exista el paquete en los repositorios actuales
def findPackageIfExist(pkg):
    return f"pacman -Ssq {pkg} | grep -w '^{pkg}'"

#habilitar network
def loadNetwork():
    subprocess.run(['systemctl', 'enable', 'systemd-networkd.service'])
     
# instala los paquetes
def installPackage(pkg):
    subprocess.call(['pacman','-S', pkg ,'--noconfirm'])
     
if __name__ == '__main__':
    updateSystemKeys()
    for pkg in subprocess.check_output(getAllInstalledPackage(), shell=True).decode('utf-8').splitlines():
        try:
            if subprocess.check_output(findPackageIfExist(pkg), shell=True):
                installPackage(pkg)
        except subprocess.CalledProcessError as e:
            print(f'\033[91mEl paquete \033[97m{pkg}\033[91m no está disponible en los repositorios oficiales\033[0m')
            exportPackageNotFound(pkg) 
        
    loadNetwork()        
    print(f'Archivo de paquetes no instalados estan \033[97m{os.path.join(os.path.expanduser("~"), "package_not_found")}\033[91m')
    
    
#!/usr/bin/env python3

import subprocess

command = ['rofi', '-theme', 'menu-theme-kumal', '-font',
           'Hack 10.5', '-show', 'drun', '-show-icons']
subprocess.run(command)
